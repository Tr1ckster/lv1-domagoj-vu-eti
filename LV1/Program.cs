﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            Zabiljeska zabiljeska1;
            zabiljeska1 = new Zabiljeska();
            Console.WriteLine(zabiljeska1.getAutor());
            Console.WriteLine(zabiljeska1.getTekst() + "\n");
            Zabiljeska zabiljeska2;
            zabiljeska2 = new Zabiljeska("Ovo je zabilješka2", "Domagoj");
            Console.WriteLine(zabiljeska2.getAutor());
            Console.WriteLine(zabiljeska2.getTekst() + "\n");
            Zabiljeska zabiljeska3;
            zabiljeska3 = new Zabiljeska("Ovo je zabilješka3", "Hrvoje", 2);
            Console.WriteLine(zabiljeska3.getAutor());
            Console.WriteLine(zabiljeska3.getTekst() + "\n");


            Zabiljeska zabiljeska4;
            zabiljeska4 = new Zabiljeska("Ovo je zabilješka4", "Ante", 3);
            Console.WriteLine(zabiljeska4.Autor);
            Console.WriteLine(zabiljeska4.Tekst);
            Console.WriteLine(zabiljeska4.Vaznost);
            string tekst = "Ovo je nova zabilješka4";
            zabiljeska4.Tekst = tekst;
            int vaznost = 1;
            zabiljeska4.Vaznost = vaznost;
            Console.WriteLine(zabiljeska4.Autor);
            Console.WriteLine(zabiljeska4.Tekst);
            Console.WriteLine(zabiljeska4.Vaznost + "\n");


            Zabiljeska zabiljeska5;
            zabiljeska5 = new Zabiljeska("Ovo je zabilješka5", "Domagoj", 1);
            Console.WriteLine(zabiljeska5.ToString() + "\n");


            VremenskaZabiljeska zabiljeska6;
            zabiljeska6 = new VremenskaZabiljeska("Ovo je zabilješka6", "Domagoj", 1);
            Console.WriteLine(zabiljeska6.ToString());
            DateTime date = new DateTime(2016, 1, 5, 4, 32, 10);
            VremenskaZabiljeska zabiljeska7;
            zabiljeska7 = new VremenskaZabiljeska("Ovo je zabilješka7", "Domagoj", 1, date);
            Console.WriteLine(zabiljeska7.ToString());
            DateTime setDate = new DateTime(1500, 9, 7);
            zabiljeska7.Vrijeme = setDate;
            Console.WriteLine(zabiljeska7.ToString());
        }
    }
}
