﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class VremenskaZabiljeska : Zabiljeska
    {
        private DateTime vrijeme;

        public VremenskaZabiljeska()
        {
            this.vrijeme = DateTime.Now;
        }
        public VremenskaZabiljeska(string tekst, string autor, int vaznost) : base(tekst, autor, vaznost)
        {
            this.vrijeme = DateTime.Now;
        }
        public VremenskaZabiljeska(string tekst, string autor, int vaznost, DateTime vrijeme) : base(tekst, autor, vaznost)
        {
            this.vrijeme = vrijeme;
        }


        public DateTime Vrijeme
        {
            get { return this.vrijeme; }
            set { this.vrijeme = value; }
        }


        public override string ToString()
        {
            return base.ToString() + "\nVrijeme nastanka: " + vrijeme.ToString();
        }
    }
}
