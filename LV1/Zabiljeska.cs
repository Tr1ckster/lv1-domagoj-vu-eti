﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Zabiljeska
    {
        private String tekst;
        private String autor;
        private int vaznost;    //manji broj - veca vaznost, 1 ima najvecu vaznost

        public string getTekst() { return this.tekst; }
        public string getAutor() { return this.autor; }
        public int getVaznost() { return this.vaznost; }

        public void setTekst(string tekst) { this.tekst = tekst; }
        public void setVaznost(int vaznost) { this.vaznost = vaznost; }

        public Zabiljeska()
        {
            this.tekst = "---------";
            this.autor = "---------";
            this.vaznost = 0;
        }
        public Zabiljeska(string tekst, string autor)
        {
            this.tekst = tekst;
            this.autor = autor;
            this.vaznost = 0;
        }
        public Zabiljeska(string tekst, string autor, int vaznost)
        {
            this.tekst = tekst;
            this.autor = autor;
            this.vaznost = vaznost;
        }

        public string Tekst
        {
            get { return this.tekst; }
            set { this.tekst = value; }
        }
        public string Autor
        {
            get { return this.autor; }
        }
        public int Vaznost
        {
            get { return this.vaznost; }
            set { this.vaznost = value; }
        }

        public override string ToString()
        {
            return this.autor + "(" + this.vaznost + ")" + ": " + this.tekst;
        }
    }
}
